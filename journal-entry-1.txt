Journal entry 1:

How's the course going?
It's ok. Some concepts are a little confusing or hard to wrap my head around it, but I think that just requires practice. 
The textbook is ok too. I found the chapter on strings hard to read. 
I do really appreciate your talk on the industry and how to get hired after the program. Coming from a non IT background
it's pretty intimidating. 

Happy with the speed of the course?
I think the speed is quick, but that's expected. I also find it hard to stay on top of the readings while working on assignments,
working on portfolios, job searches, etc. It's a lot of other volumne on top of the course itself.

I don't have any other questions, just a comment on the fast-track program itself.
After being in the program for just over a month, I feel like I'm already behind in the industry. I feel like there's
a ton of other material I will need to learn after completion of this certificate. As well, I think that this course doesn't 
go into detail on a lot of topics, so it's up to us to expand on that further. Which is fine, but it's hard to think 
about the process of getting a job afterwards when I feel like I only have a general understanding on most things. 



