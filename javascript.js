// Nice work, Kristie.  All the peices
// are here, named correctly and working
// correctly except for one crucial part...
// No matter what I enter, I always get the same
// result:  "Not a valid entry! Please try again."
// Did you add in that "else" statement right before
// you submitted without testing?  If I remove that,
// everything works fine.  To add that in properly,
// I would have checked for bad input at the beginning
// of your 'if' in whoWins() instead of at the end in the else...
// Great work otherwise
// 15/20


// globally defining these variables so we can reference them in the functions below
var gamePieces;
var userChoice;
var computerChoice;

// this function uses the Math object to return a random game piece from the gamePiece array
function getRandomGamePiece(gamePiecesLength){
  var rnd = Math.floor((Math.random() * gamePiecesLength) + 0);
  return rnd;
};

function startGame(){

    userChoice = prompt("Enter either rock, paper, scissors, or dynamite to play!").toUpperCase();
  // above we prompt the user to enter either rock, paper, scissors, or Dynamite
    gamePieces = ["ROCK", "PAPER", "SCISSORS", "DYNAMITE"];
    // here we define the items in the gamePiece array
    computerChoice= gamePieces[getRandomGamePiece(gamePieces.length)];
    // here we output what the computer choice will be
    whoWins();
};

// this function determines the results based on userChoice vs computerChoice
function whoWins(){
  var results = "";
  if (userChoice==computerChoice){
    results = "It's a tie! Play again :)"
  }
  if (userChoice=="ROCK" && computerChoice=="PAPER"){
    results="PAPER beats rock. <br/> You lose!"
  }
  if (userChoice=="ROCK" && computerChoice=="DYNAMITE"){
    results="DYNAMITE beats rock. <br/>You lose!"
  }
  if (userChoice=="ROCK" && computerChoice=="SCISSORS"){
    results="ROCK beats scissors.<br/> You win!"
  }
  if (userChoice=="PAPER" && computerChoice=="DYNAMITE"){
    results="DYNAMITE beats paper.<br/> You lose!"
  }
  if (userChoice=="PAPER" && computerChoice=="SCISSORS"){
    results="SCISSORS beats paper.<br/> You lose!"
  }
  if (userChoice=="SCISSORS" && computerChoice=="DYNAMITE"){
    results="SCISSORS beats dynamite. <br/>You win!"
  }
  // reverse order
  if (userChoice=="PAPER" && computerChoice=="ROCK"){
    results="PAPER beats rock.<br/> You win!"
  }
  if (userChoice=="DYNAMITE" && computerChoice=="ROCK"){
    results="DYNAMITE beats rock.<br/> You win!"
  }
  if (userChoice=="SCISSORS" && computerChoice=="ROCK"){
    results="ROCK beats scissors.<br/> You lose!"
  }
  if (userChoice=="DYNAMITE" && computerChoice=="PAPER"){
    results="DYNAMITE beats paper. <br/>You win!"
  }
  if (userChoice=="SCISSORS" && computerChoice=="PAPER"){
    results="SCISSORS beats paper. <br/>You win!"
  }
  if (userChoice=="DYNAMITE" && computerChoice=="SCISSORS"){
    results="SCISSORS beats dynamite.<br/> You lose!"
} else results = "Not a valid entry! Please try again."
  // we use jQuery to output our returns to our html using the id: results
  $(document).ready(function(){
    $("#results").html(results);
  });
};
